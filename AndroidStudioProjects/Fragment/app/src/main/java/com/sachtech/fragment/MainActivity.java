package com.sachtech.fragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {
    boolean status = true;
    RelativeLayout fragment_container;
    Button button;
    @Override
    protected void onCrate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                if (!status) {
                    Fragment f1 = new Fragment();
                    fragmentTransaction.add(R.id.fragment_container,f1);
                    fragmentTransaction.commit();
                    button.setText("Load Second fragmnet");
                    status = true;
                } else {
                    FragmentTwo f2 = new FragmentTwo();
                    fragmentTransaction.add(R.id.fragment_container, f2);
                    fragmentTransaction.commit();
                    button.setText("Load first fragmnet");
                    status = false;
                }

            }
        });

    }
}
