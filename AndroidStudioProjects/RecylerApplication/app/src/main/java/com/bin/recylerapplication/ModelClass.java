package com.bin.recylerapplication;

public class ModelClass {
    private int image;
    private String title;
    private String body;

    public ModelClass(int image,String title,String body) {
        this.image = image;
        this.title=title;
        this.body=body;
    }

    public int getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }
}
